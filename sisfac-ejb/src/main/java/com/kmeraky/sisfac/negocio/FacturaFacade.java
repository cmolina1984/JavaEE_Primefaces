package com.kmeraky.sisfac.negocio;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.kmeraky.sisfac.entidad.Factura;

@Stateless
public class FacturaFacade extends AbstractFacade<Factura> {
	
	@PersistenceContext(unitName = "sisfacPU")
	private EntityManager em;	
	public FacturaFacade() {
		super(Factura.class);
		
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
