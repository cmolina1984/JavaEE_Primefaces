package com.kmeraky.sisfac.entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_producto database table.
 * 
 */
@Entity
@Table(name="tipo_producto")
@NamedQuery(name="TipoProducto.findAll", query="SELECT t FROM TipoProducto t")
public class TipoProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="sec_tipo_producto", sequenceName ="tipo_producto_tippro_codigo_seq", allocationSize=1) //es necesario inicializar la secuencia
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sec_tipo_producto")
	@Column(name="tippro_codigo")
	private Long tipproCodigo;
	
	/*
	 * @SequenceGenerator(name = "SEQ",sequenceName = "login.users_id_seq",allocationSize=1)  
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "login.users_id_seq" )
	 */

	@Column(name="tippro_descripcion")
	private String tipproDescripcion;

	@Column(name="tippro_nombre")
	private String tipproNombre;

	//bi-directional many-to-one association to Producto
	@OneToMany(mappedBy="tipoProducto")
	private List<Producto> productos;

	public TipoProducto() {
	}

	public Long getTipproCodigo() {
		return this.tipproCodigo;
	}

	public void setTipproCodigo(Long tipproCodigo) {
		this.tipproCodigo = tipproCodigo;
	}

	public String getTipproDescripcion() {
		return this.tipproDescripcion;
	}

	public void setTipproDescripcion(String tipproDescripcion) {
		this.tipproDescripcion = tipproDescripcion;
	}

	public String getTipproNombre() {
		return this.tipproNombre;
	}

	public void setTipproNombre(String tipproNombre) {
		this.tipproNombre = tipproNombre;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setTipoProducto(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setTipoProducto(null);

		return producto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipproCodigo == null) ? 0 : tipproCodigo.hashCode());
		result = prime * result + ((tipproNombre == null) ? 0 : tipproNombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProducto other = (TipoProducto) obj;
		if (tipproCodigo == null) {
			if (other.tipproCodigo != null)
				return false;
		} else if (!tipproCodigo.equals(other.tipproCodigo))
			return false;
		if (tipproNombre == null) {
			if (other.tipproNombre != null)
				return false;
		} else if (!tipproNombre.equals(other.tipproNombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return tipproNombre;
	}
	

}