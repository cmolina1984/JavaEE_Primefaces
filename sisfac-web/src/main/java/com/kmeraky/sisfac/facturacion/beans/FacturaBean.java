/**
 * 
 */
package com.kmeraky.sisfac.facturacion.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.kmeraky.sisfac.core.AbstractManagedBean;
import com.kmeraky.sisfac.entidad.DetalleFactura;
import com.kmeraky.sisfac.entidad.Factura;
import com.kmeraky.sisfac.negocio.FacturaFacade;

/**
 * @author cmolina FacturaBean.java Fecha: 15 may. 2020 - 12:00:21
 */
@ManagedBean
@ViewScoped
public class FacturaBean extends AbstractManagedBean {

	private Factura factura;
	private Factura facturaSel;
	private List<Factura> listaFacturas;
	private List<DetalleFactura> listaDetalles;
	
	@EJB
	private FacturaFacade adminFactura;

	public FacturaBean() {
		this.factura = new Factura();
		this.listaFacturas = new ArrayList<>();
		this.listaDetalles = new ArrayList<>();

	}

	/**
	 * @return the factura
	 */
	public Factura getFactura() {
		return factura;
	}

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	/**
	 * @return the facturaSel
	 */
	public Factura getFacturaSel() {
		return facturaSel;
	}

	/**
	 * @param facturaSel the facturaSel to set
	 */
	public void setFacturaSel(Factura facturaSel) {
		this.facturaSel = facturaSel;
	}

	/**
	 * @return the listaFacturas
	 */
	public List<Factura> getListaFacturas() {
		return listaFacturas;
	}

	/**
	 * @param listaFacturas the listaFacturas to set
	 */
	public void setListaFacturas(List<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	/**
	 * @return the listaDetalles
	 */
	public List<DetalleFactura> getListaDetalles() {
		return listaDetalles;
	}

	/**
	 * @param listaDetalles the listaDetalles to set
	 */
	public void setListaDetalles(List<DetalleFactura> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}
	
	public void seleccionarFactura(SelectEvent ev) {
		
	}
	public void seleccionarDetalleFactura(SelectEvent ev) {
		
	}

	public void guardar() {

	}

	public void editar() {

	}

	public void eliminar() {

	}

	public void anadirDetalle() {

	}

	public void eliminarDetalle() {

	}
	private void cargarFacturas() {
		try {
			this.listaFacturas = adminFactura.consultarTodos();
			
		} catch (Exception e) {
			anadirMensajeError("No se pudo cargar las facturas: " + e.getMessage());
			
		}
		
		
		
	}

	public void resetearFormulario() {

	}
	
	
	@PostConstruct
	public void inicializar() {
		cargarFacturas();
		
		
	}

	

}
