/**
 * 
 */
package com.kmeraky.sisfac.core.convertidor;

import java.math.BigInteger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.kmeraky.sisfac.entidad.TipoProducto;
import com.kmeraky.sisfac.negocio.TipoProductoFacade;

/**
 * @author cmolina
 *ConvTipoProducto.java
 * Fecha: 13 may. 2020 - 10:59:11
 */
@ManagedBean (name = "convTipPro")
public class ConvTipoProducto implements Converter{
	
	@EJB
	private TipoProductoFacade adminTipoProducto;
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(!value.equals("")) {
			TipoProducto tipPro = adminTipoProducto.consultarPorId(Long.parseLong(value));
			return tipPro;
		}
		 return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(value != null) {
			TipoProducto tipPro = (TipoProducto) value;
			return String.valueOf(tipPro.getTipproCodigo());
			
		}else {
			return null;			
		}
		
	}

}
