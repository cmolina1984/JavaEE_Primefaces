package com.kmeraky.sisfac.admin.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.kmeraky.sisfac.core.AbstractManagedBean;
import com.kmeraky.sisfac.entidad.TipoProducto;
import com.kmeraky.sisfac.negocio.TipoProductoFacade;

/**
 * 
 * @author cmolina TipoProductoBean.java Fecha: 9 may. 2020 - 9:48:17
 */

@ManagedBean
@ViewScoped
public class TipoProductoBean extends AbstractManagedBean {

	private TipoProducto tipoProducto;
	private TipoProducto tipoProductoSel;
	private List<TipoProducto> listaTipoProducto;

	@EJB
	private TipoProductoFacade adminTipoProducto;

	public TipoProductoBean() {

		this.tipoProducto = new TipoProducto();
		this.listaTipoProducto = new ArrayList<>();
	}

	/**
	 * @return the tipoProducto
	 */
	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	/**
	 * @return the tipoProductoSel
	 */
	public TipoProducto getTipoProductoSel() {
		return tipoProductoSel;
	}

	/**
	 * @param tipoProductoSel the tipoProductoSel to set
	 */
	public void setTipoProductoSel(TipoProducto tipoProductoSel) {
		this.tipoProductoSel = tipoProductoSel;
	}

	/**
	 * @return the listaTipoProducto
	 */
	public List<TipoProducto> getListaTipoProducto() {
		return listaTipoProducto;
	}

	/**
	 * @param listaTipoProducto the listaTipoProducto to set
	 */
	public void setListaTipoProducto(List<TipoProducto> listaTipoProducto) {
		this.listaTipoProducto = listaTipoProducto;
	}

	/**
	 * 
	 */
	public void seleccionarRegistro(SelectEvent e) {
		this.tipoProductoSel = (TipoProducto) e.getObject();
	}

	public void guardar() {
		try {

			if (tipoProducto.getTipproCodigo() != null) {
				adminTipoProducto.actualizar(tipoProducto);
				anadirMensajeInfo("Registro actualizado correctamente");

			} else {

				adminTipoProducto.guardar(tipoProducto);
				anadirMensajeInfo("Registro guardado correctamente");
			}
			cargarTipoProductos();
			resetearFormulario();
		} catch (Exception e) {
			anadirMensajeError("No se ha podido guardar el registro" + e.getMessage() + " Causa:  " + e.getCause());

		}

	}

	public void editar() {

		if (this.tipoProductoSel != null) {
			this.tipoProducto = tipoProductoSel;

		} else {

			anadirMensajeError("No se ha seleccionado ningún tipos de producto");

		}

	}

	public void eliminar() {

		try {
			if (this.tipoProductoSel != null) {
				adminTipoProducto.eliminar(tipoProductoSel);
				anadirMensajeInfo("Registro eliminado correctamente");

			} else {

				anadirMensajeError("No se ha seleccionado ningún tipos de producto");

			}
			cargarTipoProductos();
			resetearFormulario();
		} catch (Exception e) {

			anadirMensajeError("No se ha podido eliminar el registro");
		}

	}

	public void resetearFormulario() {

		this.tipoProducto = new TipoProducto();
		this.tipoProductoSel = null;

	}

	private void cargarTipoProductos() {

		try {

			this.listaTipoProducto = adminTipoProducto.consultarTodos();

		} catch (Exception e) {
			anadirMensajeError("No se ha podido cargar los tipos de productos");

		}

	}

	@PostConstruct
	public void inicializar() {
		cargarTipoProductos();

	}

}
